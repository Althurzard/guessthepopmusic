//
//  ResultViewController.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
var BackToMenu: Bool = false
class ResultViewController: UIViewController {

    @IBOutlet weak var lblBestScore: CustomLabel!
    @IBOutlet weak var lblCurrentScore: CustomLabel!
    var isShowAdAlready: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        iRate.sharedInstance().promptIfNetworkAvailable()
        AdmobHelper.shareInstance.showBannerAds(viewController: self)
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if GameData.bestScore < GameData.currentScore {
            GameData.bestScore = GameData.currentScore
            EGC.reportScoreLeaderboard(leaderboardIdentifier: leaderboardID, score: GameData.bestScore)
        }
        lblBestScore.text = "\(GameData.bestScore)"
        lblCurrentScore.text = "\(GameData.currentScore)"
        showAds()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GameData.currentScore = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToGameVC(UISegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func unwindToMenuVC(UISegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func didTapShareToFB(_ sender: Any) {
        Helper.shareScore(ViewController: self)
    }

    @IBAction func didTapHomeBtn(_ sender: Any) {
        if !isShowAdAlready {
            ChartboostHelper.showInterstitial()
        }
        BackToMenu = true
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)

        
        
//       print(VC)
//        let menuVC = storyBoard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//        self.present(menuVC, animated: false, completion: nil)
    }
    
    @IBAction func didTapPlayAgainBtn(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }
 
    fileprivate func showAds(){
        isShowAdAlready = false
        let randomNum = arc4random_uniform(20)
        if randomNum % 3 == 0 {
            isShowAdAlready = true
            ChartboostHelper.showInterstitial()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
    }
    

}
