//
//  String.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/18/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit


extension String {
    func versionToInt() -> [Int] {
        return self.components(separatedBy: ".")
            .map { Int.init($0) ?? 0 }
    }
    func isVersionNewer(compareVersion: String) -> Bool {
        if self.compare(compareVersion, options: NSString.CompareOptions.numeric) == ComparisonResult.orderedDescending {
            return true
        }
        return false
    }
}
