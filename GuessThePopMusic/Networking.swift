//
//  GameHelper.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/18/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let userDefaults = UserDefaults.standard
let FAILED_FETCHING_JSON_DATA = "Failed to Load JSON DATA"
let COMPLETED_FETCHING_JSON_DATA = "Completed FETCH ALL DATA"
let CURRENT_AT_NEWEST_VERSION = "App is Up to Date"
let DOWNLOAD_PROGRESS = "Đang tải dữ liệu..\n(Chỉ tải ở lần đầu tiên)"
let PROCESSING_DATA = "Đang xử lý.."
protocol NetworkingDelegate {
    func downloadProgress(progress: CGFloat , text: String)

}
class Networking {
    var delegate: NetworkingDelegate!
    open func setDelegate(delegate: NetworkingDelegate){
        self.delegate = delegate
    }
    class var share: Networking {
        struct Static {
            static let instance = Networking()
        }
        return Static.instance
    }
    var isError = false
    open func checkCurrentVersion(){
        self.delegate.downloadProgress(progress: 0.0, text: PROCESSING_DATA)
        fetchJSONGameData(completion: {success in
            if success {
                print(COMPLETED_FETCHING_JSON_DATA)
            } else {
                print(CURRENT_AT_NEWEST_VERSION)
            }
            
        })
//        Alamofire.request("https://emotion-server.herokuapp.com/setting/version?bundle=com.superapp.notnhacvuitretrochoiamnhac&platform=ios", method: .get).validate().responseJSON(completionHandler: {response in
//            switch response.result {
//            case .success(let value):
//                let json = JSON(value)
//                
//                if let newVersion = json["result"]["version"]["versionNumber"].string {
//                    
//                    if (userDefaults.object(forKey: keyAppVersion)) == nil {
//                        self.delegate.downloadProgress(progress: 0.1, text: DOWNLOAD_PROGRESS)
//                        
//                        userDefaults.set(newVersion, forKey: keyAppVersion)
//                        
//                        self.fetchJSONGameData(version: 0){_ in 
//                            print(COMPLETED_FETCHING_JSON_DATA)
//                            userDefaults.set(0, forKey: keyDataVersion)
//                        }
//                        
//                    } else {
//                        
//                        let currentVersion = userDefaults.object(forKey: keyAppVersion) as! String
//                        
//                        if newVersion.isVersionNewer(compareVersion: currentVersion) {
//                            self.delegate.downloadProgress(progress: 0.1, text: DOWNLOAD_PROGRESS)
//                            userDefaults.set(newVersion, forKey: keyAppVersion)
//                            
//                            
//                            let version = userDefaults.object(forKey: keyDataVersion) as! Int
//                            self.fetchJSONGameData(version: version+1){ _ in
//                                print(COMPLETED_FETCHING_JSON_DATA)
//                                userDefaults.set(version+1, forKey: keyDataVersion)
//                            }
//                            
//                        } else {
//                            //Check if there are some music data haven't download yet
//                            self.delegate.downloadProgress(progress: 0.1, text: PROCESSING_DATA)
//                            self.checkData(completion: { success in
//                                if success {
//                                    print("DATA COMPLETED CHECKING")
//                                } else {
//                                    print("THERE ARE ERRORS WHILE CHECKING DATA")
//                                }
//                            })
//                        }
//                        
//                    }
//                } else {
//                    print(FAILED_FETCHING_JSON_DATA)
//                }
//                
//                break
//            case .failure(let error):
//                
//                print(error)
//                //Check if not ever download any contents
//                if userDefaults.object(forKey: keyMusicData) == nil {
//                    let alertController = UIAlertController(title: "Lỗi mạng", message: "Vui lòng kết nối mạng để tải dữ liệu lần đầu tiên", preferredStyle: .alert)
//                    let OKAction = UIAlertAction(title: "Kết nối lại", style: .default, handler: { (result: UIAlertAction) -> Void in
//                        self.checkCurrentVersion()
//                    })
//                    alertController.addAction(OKAction)
//                    if var topController = appDelegate.window?.rootViewController {
//                        while let presentedViewController = topController.presentedViewController {
//                            topController = presentedViewController
//                        }
//                        
//                        topController.present(alertController, animated: true, completion: nil)
//                    }
//                    
//                } else {
//                    self.delegate.downloadProgress(progress: 1.0, text: "")
//                }
//                
//                break
//            }
//        })
    }
  
    open func fetchJSONGameData(completion: @escaping (_ success: Bool)->()){

        var version: Int
        if userDefaults.object(forKey: keyDataVersion) == nil {
            version = 0
        } else if isError {
            //Data is incorruped, we get old version to check again
            version = userDefaults.object(forKey: keyDataVersion) as! Int
        } else {
            //check newest update on server if available
            version = 1 //userDefaults.object(forKey: keyDataVersion) as! Int + 1
        }
        
        Alamofire.request("https://emotion-server.herokuapp.com/guessmusic/data?bundle=com.superapp.notnhacvuitretrochoiamnhac&platform=iOS&version=\(version)", method: .get).validate().responseJSON(completionHandler: {response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                self.delegate.downloadProgress(progress: 0.1, text: PROCESSING_DATA)
                
                if let array = json["result"]["musicList"].array,let rootURL = json["result"]["rootURL"].url {
                    
                    var musicData: [MusicData] = []
                    var hasData = false
                    if userDefaults.object(forKey: keyMusicData) != nil {
                        hasData = true
                        let data = userDefaults.object(forKey: keyMusicData) as! Data
                        musicData = NSKeyedUnarchiver.unarchiveObject(with: data) as! [MusicData]
                    }
                
                   
                    for data in array {
 
                        if hasData {
                            var hasNewVersion = false
                            for oldData in musicData {
                                if data[keyId].string == oldData._id {
                                    oldData.replaceNewVersion(json: data)
                                    hasNewVersion = true
                                    break
                                }
                            }
                            if !hasNewVersion {
                                musicData.append(MusicData(json: data))
                            }
                        } else {
                            musicData.append(MusicData(json: data))
                        }

                    }
                    
                    
                    self.saveData(data: musicData)
                    userDefaults.set(rootURL.absoluteString, forKey: keyRootURL)
                    userDefaults.set(version, forKey: keyDataVersion)
                    userDefaults.synchronize()
                    
                } else {
                    completion(false)
                }
                
                print("Current Version: \(version)")
                
                self.checkData(completion: { success in
                    if success {
                        print("DATA COMPLETED CHECKING")
                    } else {
                        print("THERE ARE ERRORS WHILE CHECKING DATA")
                    }
                    completion(true)
                })
                
                break
            case .failure(let error):
                //Check if not ever download any contents
                if userDefaults.object(forKey: keyMusicData) == nil {
                    let alertController = UIAlertController(title: "Lỗi mạng", message: "Vui lòng kết nối mạng để tải dữ liệu lần đầu tiên", preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "Kết nối lại", style: .default, handler: { (result: UIAlertAction) -> Void in
                        self.checkCurrentVersion()
                    })
                    alertController.addAction(OKAction)
                    if var topController = appDelegate.window?.rootViewController {
                        while let presentedViewController = topController.presentedViewController {
                            topController = presentedViewController
                        }
                        
                        topController.present(alertController, animated: true, completion: nil)
                    }
                    
                } else {
                    self.delegate.downloadProgress(progress: 1.0, text: "")
                }
                completion(false)
                print(error)
                break
            }
        })
        
        
    }
    
    open func downloadData(rootURL: URL,totalDataProgress:Double, completion: @escaping (_ success: Bool)->()) {
        
     
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = documentsURL.appendingPathComponent(rootURL.lastPathComponent)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }

        let fileName = rootURL.lastPathComponent
        let downloadURL = try! (rootURL.absoluteString + "?alt=media").asURL()

        Alamofire.download(downloadURL, method: .get, to: destination).downloadProgress(closure: { (progress) in
            //progress closure
            print( "\(fileName): \(progress.fractionCompleted)")
        }).response { (response) in
            if response.error != nil {
                completion(false)
            } else {
                completion(true)
            }
        }
        
    }
    let dispatchGroup = DispatchGroup()
    var _error = false
    open func processData( musicData: [MusicData], atIndex: Int, rootURL: URL) -> [MusicData] {
        if atIndex >= musicData.count {
            return musicData
        }
        dispatchGroup.enter()
   
        let fileURL = documentsURL.appendingPathComponent(musicData[atIndex].fileName)
        if !FileManager.default.fileExists(atPath: fileURL.path) || musicData[atIndex].hasNewVersion()  {
            downloadData(rootURL: rootURL.appendingPathComponent(musicData[atIndex].fileName),
                         totalDataProgress: Double(musicData.count-1),
                         completion: { success in
                
                var notify = ""
                if success {
                    notify = "Tải '\(musicData[atIndex].title)' thành công.\n(Chỉ tải ở lần đầu tiên)"
                    print("Download \(musicData[atIndex].title) completed")
                } else {
                    self._error = true
                    
                    notify = "Tải '\(musicData[atIndex].title)' thất bại.\n(Chỉ tải ở lần đầu tiên)"
                    
                    print("Failed To Download \(musicData[atIndex].fileName)")
                }
                self.delegate.downloadProgress(progress: 0.018, text: notify)
                
                _ = self.processData(musicData: musicData, atIndex: atIndex+1, rootURL: rootURL)
                
                self.dispatchGroup.leave()
            })
        } else {
            delegate.downloadProgress(progress: 0.01, text: PROCESSING_DATA)
            
            print("\(musicData[atIndex].fileName) is Existed")
            
              _ = self.processData(musicData: musicData, atIndex: atIndex+1, rootURL: rootURL)
            
            dispatchGroup.leave()
        }
       return []
    }
    
    open func checkData(completion: @escaping (_ success: Bool)->()){

        
  
        let data = userDefaults.object(forKey: keyMusicData) as! Data
        var musicData = NSKeyedUnarchiver.unarchiveObject(with: data) as! [MusicData]
        let rootURL =  try! (userDefaults.object(forKey: keyRootURL) as! String).asURL()
        _error = false
        musicData = processData(musicData: musicData, atIndex: 0, rootURL: rootURL)
                dispatchGroup.notify(queue: DispatchQueue.main, execute: {
                    
                    print("Finished all requests.")
                    self.isError = self._error
                    if self.isError {
                        let alertController = UIAlertController(title: "Lỗi", message: "Một số tệp tin tải không thành công, bạn có muốn thử lại?", preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "Tải lại", style: .default, handler: { (result: UIAlertAction) -> Void in
                            self.delegate.downloadProgress(progress: 0.0, text: PROCESSING_DATA)
                            self.checkCurrentVersion()
                        })
                        let goOnAction = UIAlertAction(title: "Tiếp tục", style: .default, handler: { (result: UIAlertAction) -> Void in
                            self.saveData(data: musicData)
                            self.delegate.downloadProgress(progress: 1.0, text: "Hoàn tất")
                            completion(true)
                        })
                        alertController.addAction(OKAction)
                        alertController.addAction(goOnAction)
                        if var topController = appDelegate.window?.rootViewController {
                            while let presentedViewController = topController.presentedViewController {
                                topController = presentedViewController
                            }
                            
                            topController.present(alertController, animated: true, completion: nil)
                        }

                    } else {
                        self.saveData(data: musicData)
                        self.delegate.downloadProgress(progress: 1.0, text: "Hoàn tất")
                        completion(true)
                    }
                    
                    
                    return
                })
        
    }
    
    func saveData(data: [MusicData]) {
        if data.isEmpty {
            return
        }
        let archiver = NSKeyedArchiver.archivedData(withRootObject: data)
        userDefaults.set(archiver, forKey: keyMusicData)
        userDefaults.synchronize()
    }
    

}
