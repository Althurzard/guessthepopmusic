//
//  CustomButton.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
@IBDesignable
class CustomButton: UIButton {

    @IBInspectable var autoScaleLabel: Bool = true {
        didSet{
            var size: CGFloat = 17.0
            if UIScreen.isIPhone4s() {
                size = 12.0
            } else if (UIScreen.isIPhone7()){
                size = 15.0
            } else if (UIScreen.isIPhone7Plus()){
                size = 17.0
            } else if (UIScreen.isIPad97()) {
                size = 30.0
            } else if (UIScreen.isIPad124()){
                size = 48.0
            }
            self.titleLabel?.font = UIFont(name: "Helvetica Neue", size: size)
        }
    }
    
    @IBInspectable var exclusiveTap: Bool = false {
        didSet{
            self.isExclusiveTouch = exclusiveTap
        }
    }

}
