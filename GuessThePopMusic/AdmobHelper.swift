//
//  AdmobHelper.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/18/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdmobHelper {
    
    class var shareInstance: AdmobHelper {
        struct Static {
            static let instance = AdmobHelper()
        }
        return Static.instance
    }
    private var ads = GADInterstitial()
    
    var FSUnitID: String = ""
    var BannerID: String = ""
    
    func loadFullscreenAds() {
        let ad = GADInterstitial()
        ad.setAdUnitID(FSUnitID)
        
        let request = GADRequest()
        
        ad.load(request);
        
        ads = ad
        
    }
    
    func adsIsReady(viewController: UIViewController){
        if (ads.isReady)
        {
            ads.present(fromRootViewController: viewController)//Whatever  shows the ad
        }
        loadFullscreenAds()
    }
    
    //Banner Ad
    func showBannerAds(viewController: UIViewController) {
        //var adBannerView = GADBannerView()
        let adBannerView = GADBannerView(frame: CGRect(x:0, y:viewController.view.frame.size.height - 50, width:viewController.view.frame.size.width, height: 50))
        adBannerView.rootViewController = viewController
        adBannerView.adUnitID = BannerID
        adBannerView.load(GADRequest())
        
        viewController.view.addSubview(adBannerView)
        
    }
    
}

