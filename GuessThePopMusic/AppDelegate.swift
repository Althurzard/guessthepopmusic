//
//  AppDelegate.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/14/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
import OneSignal
import AdSupport
import AVFoundation
import AVKit
import AudioToolbox
import CoreData
import CoreGraphics
import CoreMedia
import CoreMotion
import CoreTelephony
import EventKit
import EventKitUI
import Foundation
import GLKit
import QuartzCore
import MediaPlayer
import MediaToolbox
import MessageUI
import MobileCoreServices
import SafariServices
import Social
import StoreKit
import SystemConfiguration
import WebKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        OneSignal.initWithLaunchOptions(launchOptions, appId: "97bb131a-03ae-43d3-a954-461c537f95ae", handleNotificationAction: nil )
        ChartboostHelper.share.start(withAppID: "588b5ed104b01644250e14cb",appSignature: "af3ed63ff28c22b3792c4144c452b96aa3dbf938")
        // Override point for customization after application launch.
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        print("App Path: \(dirPaths)")
        return true
    }
    
    override class func initialize() -> Void {
        
        //set the bundle ID. normally you wouldn't need to do this
        //as it is picked up automatically from your Info.plist file
        //but we want to test with an app that's actually on the store
        iRate.sharedInstance().onlyPromptIfLatestVersion = false
        
        //enable preview mode
        iRate.sharedInstance().appStoreID = 1207333963
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    

}

let appDelegate = UIApplication.shared.delegate as! AppDelegate

