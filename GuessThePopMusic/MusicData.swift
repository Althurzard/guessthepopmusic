//
//  MusicData.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/19/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import Foundation
import SwiftyJSON
 let keyId = "_id"
 let keyTitle = "title"
 let keyKey = "key"
 let keyFileName = "fileName"
 let keyVersion = "version"
 let keyCategory = "category"
 let keyV = "__v"
let keyNewVersion = "newversion"
class MusicData: NSObject,NSCoding {
    
    var _id: String = ""
    var title: String = ""
    var fileName: String = ""
    var currentVersion: Int = 0
    var newVersion: Int = 0
    var key: String = ""
    var category: String = ""
    var _v: Int = 0
    init(json: JSON){
        if let id = json[keyId].string {
            self._id = id
        }
        if let title = json[keyTitle].string {
            self.title = title
        }
        if let key = json[keyKey].string {
            self.key = key
        }
        if let fileName = json[keyFileName].string {
            self.fileName = fileName
        }
        if let version = json[keyVersion].int {
            self.currentVersion = version
            self.newVersion = currentVersion
        }
        if let category = json[keyCategory].string {
            self.category = category
        }
        if let v = json[keyV].int {
            self._v = v
        }
    }
    
    
    init(id: String, title: String, fileName: String, version: Int, newVersion: Int, key: String, category: String, v: Int) {
       self._id = id
        self.title = title
        self.fileName = fileName
        self.currentVersion = version
        self.category = category
        self._v = v
        self.key = key
        self.newVersion = newVersion
    }
    required convenience init(coder aDecoder: NSCoder) {
        let id = aDecoder.decodeObject(forKey: keyId) as! String
        let title = aDecoder.decodeObject(forKey: keyTitle) as! String
        let fileName = aDecoder.decodeObject(forKey: keyFileName) as! String
        let version = aDecoder.decodeInteger(forKey: keyVersion)
        let key = aDecoder.decodeObject(forKey: keyKey) as! String
        let category = aDecoder.decodeObject(forKey: keyCategory) as! String
        let v = aDecoder.decodeInteger(forKey: keyV)
        let _newVersion = aDecoder.decodeInteger(forKey: keyNewVersion)
        self.init(id: id, title: title, fileName: fileName, version: version, newVersion: _newVersion, key: key, category: category, v: v)
    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_id, forKey: keyId)
        aCoder.encode(title, forKey: keyTitle)
        aCoder.encode(key, forKey: keyKey)
        aCoder.encode(currentVersion, forKey: keyVersion)
        aCoder.encode(category, forKey: keyCategory)
        aCoder.encode(_v, forKey: keyV)
        aCoder.encode(fileName, forKey: keyFileName)
        aCoder.encode(newVersion,forKey: keyNewVersion)
    }
    
    func replaceNewVersion(json: JSON){
        if let id = json[keyId].string {
            self._id = id
        }
        if let title = json[keyTitle].string {
            self.title = title
        }
        if let key = json[keyKey].string {
            self.key = key
        }
        if let fileName = json[keyFileName].string {
            self.fileName = fileName
        }
        if let version = json[keyVersion].int {
            self.newVersion = version
     
        }
        if let category = json[keyCategory].string {
            self.category = category
        }
        if let v = json[keyV].int {
            self._v = v
        }
    }
    
    func hasNewVersion() -> Bool {
        if currentVersion < newVersion {
            currentVersion = newVersion
            return true
        }
        return false
    }
    
    func play(){
        SKTAudio.sharedInstance().playBackgroundMusic(filename: fileName)
    }
}
