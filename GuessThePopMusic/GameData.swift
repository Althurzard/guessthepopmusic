//
//  GameData.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/18/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit

fileprivate let key = "bestscore"

struct GameData {

    static var currentScore: Int = 0
    static var bestScore: Int {
        set (newValue){
            UserDefaults.standard.set(newValue, forKey: key)
        }
        get{
            if UserDefaults.standard.object(forKey:key) != nil {
                return UserDefaults.standard.integer(forKey: key)
            } else {
                return 0
            }
        }
    }
}
