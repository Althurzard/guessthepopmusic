//
//  RoundedUIView.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedUIView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 5 {
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }

}
