//
//  LoadingViewController.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/19/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController,NetworkingDelegate {

    @IBOutlet weak var lblDownloadProcess: CustomLabel!
    @IBOutlet weak var loadingProgressBar: UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Networking.share.setDelegate(delegate: self)
        Networking.share.checkCurrentVersion()
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func downloadProgress(progress: CGFloat, text: String) {
        var newProgress:Float = loadingProgressBar.progress + Float(progress)
        
        if newProgress >= 1.0 {
            newProgress = 1.0
            
            Helper.delay(seconds: 0.5){
                
                self.performSegue(withIdentifier: "goToMenuVC", sender: self)
            }
        }
        if (progress == 0.0){
            newProgress = Float(progress)
        }
        loadingProgressBar.setProgress(newProgress, animated: true)
        lblDownloadProcess.text = text
        
    }
}
