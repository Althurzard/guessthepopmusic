//
//  GameViewController.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
let fixedTime: CGFloat = 10.0

class GameViewController: UIViewController {

    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var btnAnswer1: UIButton!
    @IBOutlet weak var btnAnswer2: UIButton!
    @IBOutlet weak var btnAnswer3: UIButton!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var diskImage: UIImageView!
    
    @IBOutlet weak var faceResultImage1: UIImageView!
    @IBOutlet weak var faceResultImage2: UIImageView!
    @IBOutlet weak var faceResultImage3: UIImageView!
    
    @IBOutlet weak var btnAnswer3ConstraintY: NSLayoutConstraint!
    @IBOutlet weak var containProgressBarView: RoundedUIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AdmobHelper.shareInstance.showBannerAds(viewController: self)
        drawProgressLayer()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if BackToMenu {
            return
        }

        
        let data = userDefaults.object(forKey: keyMusicData) as! Data
        musicData = NSKeyedUnarchiver.unarchiveObject(with: data) as! [MusicData]
        
        
        self.adjustAlpha()
        self.adjustConstraint()
        
    
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if BackToMenu {
            return
        }
        
        initGame()
        showUI()
        rotateImage(target: diskImage)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        initRectProgress(incremented: progressWidth)
        SKTAudio.sharedInstance().pauseBackgroundMusic()
        faceResultImage1.isHidden = true;
        faceResultImage2.isHidden = true;
        faceResultImage3.isHidden = true;
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    var borderLayer : CAShapeLayer = CAShapeLayer()
    let progressLayer : CAShapeLayer = CAShapeLayer()
    
    var isTouching: Bool = false
    var chosenListMusic : [MusicData] = []
    var chosenMusic: MusicData!
    var isCorrect: Bool = false
    var time: Int = Int(fixedTime)
    var milisecond : CGFloat = fixedTime
    var musicData : [MusicData] = []
    var timer = Timer()
    var progressBarTimer = Timer()
    
    
    lazy var progressWidth : CGFloat = {
        return self.containProgressBarView.bounds.width*0.95
    }()
    
    
    
    
    
    
    
    //MARK: GAME LOGIC
    
    fileprivate func initGame(){
        Helper.delay(seconds: 0.5){
            self.faceResultImage1.isHidden = true
            self.faceResultImage2.isHidden = true
            self.faceResultImage3.isHidden = true
            
            self.milisecond = fixedTime
            self.time = Int(fixedTime)
            self.playRandomMusic()
            
            self.startTimer()
            self.lblScore.text = "\(GameData.currentScore)"
            self.lblTimer.text = "\(self.time)"
            
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.startTimer), userInfo: nil, repeats: true)
            
            self.progressBarTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.rectProgress(incremented:)), userInfo: nil, repeats: true)
        }
        
    }
    
    fileprivate func playRandomMusic(){
        
        var index: UInt32 = UInt32(musicData.count - 1)
        guard index >= 3 else {
            SKTAudio.sharedInstance().playSoundEffect(filename: "Explosion2.wav")
            endGame(isCorrect: false)
            return
        }
        
        //get the three random songs in the list to the temporary list
        let dispatchGroup = DispatchGroup()
        var n = 3
        for _ in 0..<n {
            dispatchGroup.enter()
            index = UInt32(musicData.count - 1)
            let newIndex = Int(arc4random_uniform(index))
            let data = musicData[newIndex]
            var flag = false
            for music in chosenListMusic {
                //avoid has a same answer
                if music._id == data._id {
                    musicData.remove(at: newIndex)
                    flag = true
                    break
                }
            }
            if flag {
                n += 1
                continue
            }
            chosenListMusic.append(data)
            musicData.remove(at: newIndex)
            print(data.title)
            
            dispatchGroup.leave()
        }
        dispatchGroup.notify(queue: DispatchQueue.main, execute: {
            self.btnAnswer1.setTitle(self.chosenListMusic[0].title, for: .normal)
            self.btnAnswer2.setTitle(self.chosenListMusic[1].title, for: .normal)
            self.btnAnswer3.setTitle(self.chosenListMusic[2].title, for: .normal)
            
            //the song chosen to play in the temporary list
            let randomSongIndex = arc4random_uniform(UInt32(self.chosenListMusic.count - 1))
            self.chosenMusic = self.chosenListMusic[Int(randomSongIndex)]
            self.chosenListMusic.remove(at: Int(randomSongIndex))
            
            
            print("\(self.chosenMusic.title) is playing")
            self.chosenMusic.play()
        })
       
    }
    
    fileprivate func checkWhetherCorrectOrIncorrect(Button: UIButton) -> Bool {
        
        isTouching = true
        var flag = false
        if Button.titleLabel?.text! == chosenMusic.title {
            flag = true
            isCorrect = true
            
            SKTAudio.sharedInstance().playSoundEffect(filename: "beep.wav")
            
            for data in chosenListMusic {
               
                musicData.append(data)
            }
            chosenListMusic.removeAll()
            
            lblScore.text = "\(GameData.currentScore+=time)"
            
            
        } else {
            isCorrect = false
            SKTAudio.sharedInstance().playSoundEffect(filename: "Explosion2.wav")
        }
        
        switchFaceResult(Tag: Button.tag, isCorrect: flag)
        return flag
    }
    
    fileprivate func endGame(isCorrect: Bool){
        timer.invalidate()
        progressBarTimer.invalidate()
        Helper.delay(seconds: 0.5) {
            self.isTouching = false
            if isCorrect {
                
                self.initGame()
                self.view.setNeedsDisplay()
            } else {
                
                self.performSegue(withIdentifier: "goToResultVC", sender: self)
            }
        }
    }
    
    fileprivate func switchFaceResult(Tag: Int, isCorrect: Bool){
        var imageName = ""
        if isCorrect {
            imageName = "right.png"
        } else {
            imageName = "wrong.png"
        }
        switch Tag {
        case 1:
            faceResultImage1.image = UIImage(named: imageName)
            faceResultImage1.isHidden = false
            break
        case 2:
            
            faceResultImage2.image = UIImage(named: imageName)
            faceResultImage2.isHidden = false
            break
        case 3:
            faceResultImage3.image = UIImage(named: imageName)
            faceResultImage3.isHidden = false
            break
        default: break
        }
    }
    
    
    //MARK: TAP'S EVENTS
    
    @IBAction func didTapAnswer3Btn(_ sender: Any) {
        guard !isTouching else {
            return
        }
        endGame(isCorrect: checkWhetherCorrectOrIncorrect(Button: btnAnswer3))
    }
   
    @IBAction func didTapAnswer2Btn(_ sender: Any) {
        
        guard !isTouching else {
            return
        }

        endGame(isCorrect: checkWhetherCorrectOrIncorrect(Button: btnAnswer2))
    }
    
    
    @IBAction func didTapAnswer1Btn(_ sender: Any) {
        
        guard !isTouching else {
            return
        }
        endGame(isCorrect: checkWhetherCorrectOrIncorrect(Button: btnAnswer1))

    }
    
    
    //MARK: ANIMATION
    
   
    
    fileprivate func rotateImage(target: UIView, duration: TimeInterval = 1.0){
        UIView.animate(withDuration: duration, delay: 0.0, options: [.repeat,.curveLinear], animations: {
            target.transform = target.transform.rotated(by: CGFloat(M_PI))
        }, completion: nil)

    }
    
    //MARK: PROGRESS BAR
    fileprivate func drawProgressLayer(){
        print(containProgressBarView.bounds)
        let bezierPath = UIBezierPath(roundedRect: containProgressBarView.bounds, cornerRadius: containProgressBarView.cornerRadius)
        bezierPath.close()
        borderLayer.path = bezierPath.cgPath
        borderLayer.fillColor = UIColor.black.cgColor
        borderLayer.strokeEnd = 0
        containProgressBarView.layer.addSublayer(borderLayer)
        
    }
    
    @objc fileprivate func rectProgress(incremented : CGFloat = 0.0){
        guard self.time != 0 else {return}
        self.milisecond -= 0.115
        let percent = (fixedTime - self.milisecond)/10
        let progress: CGFloat =  percent*self.progressWidth
        let increment = self.progressWidth - progress
        
        
        initRectProgress(incremented: increment)
    }
    
    func initRectProgress(incremented: CGFloat) {
        if incremented <= progressWidth{
            
            progressLayer.removeFromSuperlayer()
            let bezierPathProg = UIBezierPath(roundedRect: CGRect(x: 5, y: 5, width: incremented, height: containProgressBarView.bounds.height - 10) , cornerRadius: containProgressBarView.cornerRadius)
            bezierPathProg.close()
            progressLayer.path = bezierPathProg.cgPath
            progressLayer.fillColor = UIColor.yellow.cgColor
            borderLayer.addSublayer(progressLayer)
        }
    }
    
    //MARK: TIMER
    
    @objc fileprivate func startTimer(){
        self.time -= 1
        self.lblTimer.text = "\(self.time)"
        guard self.time != 0 else {
            SKTAudio.sharedInstance().playSoundEffect(filename: "Explosion2.wav")
            self.diskImage.layer.removeAllAnimations()
           self.endGame(isCorrect: false)
            return
        }

    }
    
    
    //MARK: ANIMATION
    
    fileprivate func showUI(constant: CGFloat = 50, alpha: CGFloat = 1.0, duration: TimeInterval = 1.0, completion:((_ success: Bool)->Swift.Void)? = nil){
        self.adjustConstraint(constant: constant)
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.0, options: [], animations: {
            //display button
            self.view.layoutIfNeeded()
            self.adjustAlpha(alpha: alpha)
            
        }, completion: nil)
        
    }
    
    fileprivate func adjustConstraint(constant: CGFloat = -50){
        self.btnAnswer3ConstraintY.constant += constant
    }
    
    fileprivate func adjustAlpha(alpha: CGFloat = 0.0){
        self.btnAnswer1.alpha = alpha
        self.btnAnswer2.alpha = alpha
        self.btnAnswer3.alpha = alpha
    }
    
    fileprivate func hideUI(flag: Bool){
        self.btnAnswer1.isHidden = flag
        self.btnAnswer2.isHidden = flag
        self.btnAnswer3.isHidden = flag
    }

    @IBAction override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
}
