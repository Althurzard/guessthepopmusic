//
//  ChartboostHelper.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/18/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import Foundation


class ChartboostHelper: NSObject, ChartboostDelegate {
    
    class var share: ChartboostHelper {
        struct Static {
            static let instance = ChartboostHelper()
        }
        return Static.instance
    }
    open func start(withAppID: String,appSignature: String){
        Chartboost.start(withAppId: withAppID, appSignature: appSignature, delegate: self)
    }
    
    class func showInterstitial(){
        if Chartboost.hasInterstitial(CBLocationHomeScreen) {
            Chartboost.showInterstitial(CBLocationHomeScreen)
        } else {
            Chartboost.cacheInterstitial(CBLocationHomeScreen)
        }
    }
    
    internal func didCloseInterstitial(_ location: String!) {
        
        if location == CBLocationHomeScreen {
            
            Chartboost.cacheInterstitial(CBLocationHomeScreen)
        }
    }
    
}
