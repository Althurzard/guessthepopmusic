//
//  Define.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit

let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
let keyAppVersion = "appVersion"
let keyDataVersion = "dataVersion"
let keyMusicData = "music_data"
let keyRootURL = "RootURL"
let leaderboardID = "com.superapp.notnhacvuitretrochoiamnhac.highscore"
let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
let kTapdaqAppID = "58ab9880dc7714002fe6b185"
let kTapdaqClientID = "95f08d94-5156-44e7-ab43-4d4180034043"
let kTapdaPlacementTag = "Default"


class Helper {
    /**
     Delay for any second and excute closure
     - parameter seconds: wait for specific seconds
     - parameter closure: excute an block
     */
    class func delay(seconds:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: closure)
    }
    
    /**
     This is used for rate app in-game
     - parameter appId: the app ID on the appstore
     - parameter completion: callback
     */
    class func rateApp(appId: String, completion: ((_ success: Bool)->Swift.Void)? = nil  ) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion?(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion?(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    /**
     This is used for capture an picture on screen and share or save to user's device
     - parameter ViewController: the current view controller
     */
    class func shareScore(ViewController: UIViewController, completions: ((_ success:Bool)->Swift.Void)? = nil) {
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: ViewController.view!.frame.width, height: ViewController.view!.frame.height), true, 1)
        
        ViewController.view!.drawHierarchy(in: ViewController.view!.frame, afterScreenUpdates: true)
        
        // retrieve graphics context
        let _ = UIGraphicsGetCurrentContext()
        
        // query image from it
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        let activityViewController = UIActivityViewController(activityItems: [image!], applicationActivities: nil)
        
        ViewController.present(activityViewController, animated: true, completion:{
            if completions != nil {
                completions!(true)
            }
        })
        
    }

}

