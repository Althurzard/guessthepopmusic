//
//  UIScreen.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/17/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit

extension UIScreen {
    class func isIPhone4s () -> Bool{
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 568.0
    }
    class func isIPhone7 () -> Bool {
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 667.0
    }
    class func isIPhone7Plus () -> Bool {
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 736.0
    }
    class func isIPad97 () -> Bool {
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 1024.0
    }
    class func isIPad124 () -> Bool {
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 1366.0
    }
}
