//
//  ViewController.swift
//  GuessThePopMusic
//
//  Created by Nguyen Quoc Vuong on 2/14/17.
//  Copyright © 2017 Althurzard. All rights reserved.
//

import UIKit
import GameKit

class MenuViewController: UIViewController,EGCDelegate {

    @IBOutlet weak var btnHotGame: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnRateThisApp: UIButton!
    @IBOutlet weak var btnAbout: UIButton!
    @IBOutlet weak var btnLeaderboard: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var rateThisAppContraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let window = UIApplication.shared.windows[0] as UIWindow;
        window.rootViewController = self;
        _ = EGC.sharedInstance(self)
        ChartboostHelper.showInterstitial()
        
        AdmobHelper.shareInstance.BannerID = "ca-app-pub-6079114901814202/8025865975"
        AdmobHelper.shareInstance.showBannerAds(viewController: self)
        SKTAudio.sharedInstance().playBackgroundMusic(filename: "PianoWin.wav")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.adjustAlpha(alpha: 0.0)
        self.adjustConstraint(constant: -50)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showUI(constant: 50, alpha: 1.0, duration: 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        BackToMenu = false
        SKTAudio.sharedInstance().pauseBackgroundMusic()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: BUTTONS
    
    @IBAction func didTapPlayBtn(_ sender: Any) {

        
    }
    
    @IBAction func didTapHotGameBtn(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "http://www.bestappsforphone.com/iosgameofthemonth")! as URL)
        
    }
    
    @IBAction func didTapAboutBtn(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "http://www.bestappsforphone.com")! as URL)
    }
    
    @IBAction func didTapLeaderboardBtn(_ sender: Any) {
        showLeaderboard()
    }

    @IBAction func didTapRateThisAppBtn(_ sender: Any) {
        Helper.rateApp(appId: "id1207333963")
    }
    
    //MARK: ANIMATION
    
    fileprivate func showUI(constant: CGFloat, alpha: CGFloat, duration: TimeInterval, completion:((_ success: Bool)->Swift.Void)? = nil){
        self.adjustConstraint(constant: constant)
        UIView.animate(withDuration: duration+1, delay: 0.15, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.0, options: [], animations: {
            //display button
            self.view.layoutIfNeeded()
            self.adjustAlpha(alpha: alpha)
            
        }, completion: nil)
        
    }
    
    fileprivate func adjustConstraint(constant: CGFloat){
        self.rateThisAppContraint.constant += constant
    }
    
    fileprivate func adjustAlpha(alpha: CGFloat){
        self.btnPlay.alpha = alpha
        self.btnAbout.alpha = alpha
        self.btnHotGame.alpha = alpha
        self.btnLeaderboard.alpha = alpha
        self.btnRateThisApp.alpha = alpha
    }
    
    fileprivate func hideUI(flag: Bool){
        self.btnPlay.isHidden = flag
        self.btnAbout.isHidden = flag
        self.btnHotGame.isHidden = flag
        self.btnLeaderboard.isHidden = flag
        self.btnRateThisApp.isHidden = flag
    }

    fileprivate func showLeaderboard(){
        
        EGC.showGameCenterLeaderboard(leaderboardIdentifier: leaderboardID)
    }
    
    func EGCAuthentified(_ authentified: Bool) {
        if authentified {
            EGC.getGKScoreLeaderboard(leaderboardIdentifier: leaderboardID) {
                (resultGKScore) -> Void in
                if let resultGKScoreIsOK = resultGKScore as GKScore? {
                    print("\n[Easy Game Center] Hight Score : \(resultGKScoreIsOK.value)\n")
                    GameData.bestScore = Int(resultGKScoreIsOK.value)
                }
            }
        }
    }
    
    
}

